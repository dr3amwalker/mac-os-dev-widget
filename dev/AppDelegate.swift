//
//  AppDelegate.swift
//  dev
//
//  Copyright © 2015 Alex Astakhov. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    @IBOutlet weak var statusMenu: NSMenu!
    
    let statusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-1);

    let iconGray = NSImage(named: "statusIconGrayed")
    
    let iconBlack = NSImage(named: "statusIcon")
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        statusItem.image = iconGray
        statusItem.menu = statusMenu
    }

    @IBAction func showHiddenFilesButtonClick(sender: NSMenuItem) {
        let task = NSTask()
        task.launchPath = "/usr/bin/defaults"
        
        sender.state = sender.state == NSOnState ? NSOffState : NSOnState
        task.arguments = ["write", "com.apple.finder", "AppleShowAllFiles", sender.state == NSOnState ?"YES":"NO"]
        task.launch()
        task.waitUntilExit()
        
        let killTask = NSTask()
        killTask.launchPath = "/usr/bin/killall"
        killTask.arguments = ["Finder"]
        killTask.launch()
    }
    
    @IBAction func enableFunctionKeysButtonClick(sender: NSMenuItem) {
        if let path = NSBundle.mainBundle().pathForResource("fn", ofType:"scpt") {
            let task = NSTask()
            task.launchPath = "/usr/bin/osascript"
            sender.state = sender.state == NSOnState ? NSOffState : NSOnState
            task.arguments = [path]
            task.launch()
            task.waitUntilExit()
            statusItem.image = sender.state == NSOnState ? iconBlack : iconGray
        }
    }
    
    @IBAction func enableProgrammersModeButtonClick(sender: NSMenuItem) {
        let menu = sender.menu
        if (sender.title == "Enable programmers mode") {
            sender.title = "Disable programmers mode"
        }
        else {
            sender.title = "Enable programmers mode"
        }
        enableFunctionKeysButtonClick((menu?.itemWithTag(2))!)
    }
    
    @IBAction func quitButtonClick(sender: NSMenuItem) {
        exit(0)
    }

}

