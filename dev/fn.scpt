-- Taken from https://gist.github.com/alampros/2b81af6cfc564f6accd3
-- Script contents are listed below:

-- Originally posted by "kiodane" at http://forums.macrumors.com/showthread.php?t=383969
tell application "System Preferences"
	set current pane to pane "com.apple.preference.keyboard"
end tell

tell application "System Events"
	if UI elements enabled then
		tell tab group 1 of window "Keyboard" of process "System Preferences"
			click checkbox "Use all F1, F2, etc. keys as standard function keys"
			if (do shell script "defaults read -g com.apple.keyboard.fnState") = "1" then
				set fnStateRead to "Function Keys - F1, F2, F3..."
				set fnStateMessageRead to "F keys are now real function keys."
			else
				set fnStateRead to "Special Keys - Volume, iTunes..."
				set fnStateMessageRead to "F keys are now special media keys."
			end if
			do shell script "/usr/local/bin/terminal-notifier -message \"" & fnStateMessageRead & "\" -title \"" & fnStateRead & "\"  -activate com.orderedbytes.ControllerMate4"
		end tell
		tell application "System Preferences"
			quit
		end tell
	else
		tell application "System Preferences"
			set current pane ¬
				to pane "com.apple.preference.security"
			display dialog ¬
				"Accessibility control is not enabled. Under Privacy => Accessibility, check the box next to the name of this application."
		end tell
	end if
end tell
